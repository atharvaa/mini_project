#include"stack.h"
#include<stdlib.h>
void initopnd(opndstack *s) {
	*s = NULL;
}

void pushopnd(opndstack *s, Number num) {
	node1 *tmp = (node1 *) malloc(sizeof(node1));
	tmp->val = num;
	tmp->next = *s;
	*s = tmp;
}

Number popopnd(opndstack*s) {
	Number temp;
	temp = (*s)->val;
	(*s)= (*s)->next;
	return temp;
}

int isemptyopnd(opndstack *s) {
	return *s == NULL;
}
int isfullopnd(opndstack *s) {
	return 0;
} 

void initoptr(optrstack *s) {
	*s = NULL;
}

void pushoptr(optrstack *s, char c) {
	node2 *tmp = (node2 *) malloc(sizeof(node2));
	tmp-> ch = c;
	tmp->next = *s;
	*s = tmp;
}

char popoptr(optrstack*s) {
	char temp;
	node2 *tmp;
	temp = (*s)-> ch;
	tmp = *s;
	(*s)= (*s)->next;
	free(tmp);
	return temp;
}

int isemptyoptr(optrstack s) {
	return s == NULL;
}
int isfulloptr(optrstack *s) {
	return 0;
} 


#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"stack.h"
#include<string.h>
/*	Read input from user
*	Verify character is digit or operater
*	Write function for precedance
*	Write function for evaluation
*	Think about braces
*/

int optrchar(char ch) {
	switch(ch) {
		case '+':
		case '-':
		case '*':
		case '/':
		case '^':
		case '%':
			return 1;
		default:
			return 0;
	}
}
/* Evaluation step by step */
Number eval(char ch, Number opnd1, Number opnd2) {
	switch(ch) {
	case '+':
		return addnumbers(opnd1, opnd2);
	case '-':
		return subtractnumbers(opnd1, opnd2);
	case '*':
		return multipliaction(opnd1, opnd2);
	case '/':
		return division(opnd1, opnd2);
	case '^':
		return NULL;
	case '%':
		return NULL;
	default:
		printf("Invalid operation:");
		return NULL;
	}
}

int isSign(char str) {
	if(str == '+' || str == '-')
		return 1;
	else
		return 0;
}
/*	Solving Program function 	*/
Number prefix(char **str) {
	Number opnd[2], value, num1 = NULL;
	char ch, seq[35];
	int length, i = 0, j;
	char *saveptr, *ptr;
	opndstack opndstk, reverse;
	optrstack optrstk;
	initoptr(&optrstk);
	initopnd(&opndstk);
	initopnd(&reverse);
	while(**str != '\0') {
		if(isDigit(**str) || (isSign(**str) && isDigit(*(*str + 1)))){
			ptr = (char *)malloc((strlen(*str)+1) * sizeof(char));
			strcpy(ptr, *str);
			strtok_r(ptr, " ", &saveptr);
			length = strlen(ptr);
			(*str) = (*str) + length;
			storenum(&num1, ptr);
			if(num1 == NULL){
				printf("error at point p");
				return NULL;
			}
			pushopnd(&opndstk, num1);
			seq[i++] = 'n';	// reminder for operand
			free(ptr);
		} 
		else if(optrchar(**str)) {
			pushoptr(&optrstk, **str);
			seq[i++] = 'c'; // reminder for operater
		}
		else if(!(**str == ' ' || **str == '\t')){
			printf("Wronge syntax at point '%c'", **str);
			return NULL;
		}
		(*str)++;
	}
	/* solve in prefix order */
	while(i > 0){
		switch(ch = seq[--i])
		{
			case 'n':
				pushopnd(&reverse, popopnd(&opndstk));
			break;
			case 'c':
				for(j = 0; j <= 1; j++){
					if(!(isemptyopnd(&reverse))){
						opnd[j] = popopnd(&reverse);
					}
					else{
						return NULL;
					}
				}
				ch = popoptr(&optrstk);
				value = eval(ch, opnd[0], opnd[1]);
				if(value == NULL)
					return NULL;
				pushopnd(&reverse, value);
				destroy(opnd+0);
				destroy(opnd+1);
			break;
		}
	}
	value = popopnd(&reverse);
	if(isemptyopnd(&reverse))
		return value;
	return NULL;
}

int main() {
	int i;
	char ch;
	char *str, *ptr;
	Number num;
	printf("This is the prefix bc program\nfor user guide type 'help'\nTo exit type 'q'\n");
	while(1){
		i = 0;
		for(i = 0; (ch = getchar()) != '\n';){
			if(i == 0){
				str = (char *)malloc(sizeof(char));
			}
			str[i++] = ch;
			str = (char *)realloc(str, (i+2) * (sizeof(char)));	
		}
		if(i == 0 && ch == '\n')
		    continue;
		str[i] = '\0';
		ptr = str;
		if(strcmp(str, "q") == 0){
		    // it's success to close program
			exit(EXIT_SUCCESS);
		}
	/*	if(strstr(str, "scale=")){
			strtok_r(str, "=", &saveptr);
			scale = atoi(saveptr);
		}
	*/
		// string is send to solve expression return NULL,if unable to solve i.e Invalid Input by user
		else{
			str = ptr;
			num = prefix(&str);
			if(num == NULL){
				printf("syntax error\n");
				continue;
			}
			printNumber(num);
		}
		free(ptr);
	}
	return 0;
}
